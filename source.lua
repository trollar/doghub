-- certified dog hub classic
local function request(url)
    local result = game:HttpGet(url)
    return result
end

local function loadcode(code, returnresult)
    local success, err = pcall(function()
        loadstring(code)()
    end)
    if returnresult then 
        return success
    else
        return nil
    end
end

loadcode("print('doghub')", false) -- it does do